#Verifica si los archivos son iguales y si no los modifica
import os

with open ("/home/hugo/monitoreo/estado-de-la-red/index.html", "r+") as archivo_web:
    datos_web = archivo_web.readlines()

datos_web.pop(0)
datos_web.pop(-1)
print (datos_web)

with open ("/home/hugo/monitoreo/estado-de-la-red/index_local.html", "r+") as archivo_local:
    datos_local = archivo_local.readlines()


    
datos_local.pop(0)
datos_local.pop(-1)


if datos_local == datos_web:
    print ("Son iguales")
else:
    print ("Hubo modificaciones en la red")
    os.system ('cp index_local.html index.html')
    os.system('git pull')
    os.system('git add .')
    os.system('git commit -m "Hubo modificaciones en el estado de la red"')
    os.system ('git push origin main')
