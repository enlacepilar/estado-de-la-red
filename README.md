## Monitor de red - Estado de la red

Si algo explota (bueno, tampoco es para tanto) me lo va a avisar en una web de Netlify.

La idea es esta:

* En un servidor Ubuntu hace un ping con Python, programado con Crontab cada 10 minutos, ponele 

* Esos datos los guarda en un archivo de texto, vendría a ser como una especie de Log

* A su vez guarda en un archivo HTML las condiciones de la red. 

* Además lee ese archivo y se fija si en el texto algo cambió. Si lo hizo, cambia el estado de la red y hace un commit a Gitlab, cosa de que no lo esté haciendo a cada rato como loco malo.

* En Netlify se actualiza solo así que cuando me conecto desde cualquier lado puedo ver en qué estado está todo

A disfrutar y relajarse. :)
